ExternalProject_Add(NanoServer
    GIT_REPOSITORY https://bitbucket.org/nano-dimension/nanoserver
    GIT_TAG origin/master
    GIT_SHALLOW 1
    STEP_TARGETS update
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNALPROJECT_INSTALL_PREFIX}
)


add_dependencies(update NanoServer)