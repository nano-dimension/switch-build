@echo OFF

set cbe_install_dir=C:\switch-build-environment
set CMAKE_PREFIX_PATH=%cbe_install_dir%
set CURA_BUILD_SRC_PATH=C:\switch-build-environment\switch-build
set CURA_INSTALL_OUTPUT_PATH=C:\switch-build-environment\output
set CPACK_GENERATOR=NSIS

setx CURA_VERSION_MAJOR 1
setx CURA_VERSION_MINOR 0
setx CURA_VERSION_PATCH 2
setx CURA_VERSION_EXTRA "BN-0"
setx CURA_BRANCH_OR_TAG "Development"'

echo ========== Env Variables BEGIN ==========
set
echo ========== Env Variables END ==========

echo =========== Deleting output folders BEGIN ===========
@RD /S /Q C:\switch-build-environment\output
@RD /S /Q c:\Build
echo =========== Deleting output folders END ===========

cd \switch-build-environment\switch-build
scripts\python3.5\windows\build_in_docker_vs2015.cmd