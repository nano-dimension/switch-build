find_package(cx_freeze 5.0 REQUIRED)

configure_file(${CMAKE_CURRENT_LIST_DIR}/setup_win32.py.in setup.py @ONLY)
add_custom_target(build_bundle)
add_dependencies(packaging build_bundle)
add_dependencies(build_bundle projects)

add_custom_command(
    TARGET build_bundle PRE_LINK
    COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_BINARY_DIR}/package
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_BINARY_DIR}/package
    COMMENT "cleaning old package/ directory"
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_command(
    TARGET build_bundle POST_BUILD
    COMMAND ${Python3_EXECUTABLE} setup.py build_exe
    COMMENT "running cx_Freeze"
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)

add_custom_command(
    TARGET build_bundle POST_BUILD
    # NOTE: Needs testing here, whether CPACK_SYSTEM_NAME is working good for 64bit builds, too.
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/packaging/switch.ico ${CMAKE_BINARY_DIR}/package/
    COMMAND ${CMAKE_COMMAND} -E rename ${CMAKE_BINARY_DIR}/package/switch.ico ${CMAKE_BINARY_DIR}/package/switch.ico
    COMMENT "copying switch.ico as switch.ico into package/"
)

add_custom_command(
    TARGET build_bundle POST_BUILD
    # NOTE: Needs testing here, whether CPACK_SYSTEM_NAME is working good for 64bit builds, too.
    COMMAND ${CMAKE_COMMAND} -E copy_directory  ${EXTERNALPROJECT_INSTALL_PREFIX}/Engine ${CMAKE_BINARY_DIR}/package/Engine/    
    COMMAND ${CMAKE_COMMAND} -E copy_directory  ${CMAKE_SOURCE_DIR}/../flight-control/artworkengine/WCAD ${CMAKE_BINARY_DIR}/package/Engine/WCAD
    # install(DIRECTORY ${EXTERNALPROJECT_INSTALL_PREFIX}/arduino
    #         DESTINATION "."
    #         COMPONENT "arduino"
    # )
    COMMENT "copying Engine to Package"
)

#
# CURA-6074
# QTBUG-57832
# Patch Qt dialogplugin.dll to avoid adding all available drives as shortcuts for FileDialog.
#
if(BUILD_OS_WINDOWS)
    add_custom_command(
        TARGET build_bundle POST_BUILD
        # NOTE: Needs testing here, whether CPACK_SYSTEM_NAME is working good for 64bit builds, too.
        COMMAND ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/packaging/patch_qt5.10_dialogplugin.py ${CMAKE_BINARY_DIR}/package
        COMMENT "CURA-6074 Patching dialogplugin.dll in ${CMAKE_BINARY_DIR}/package"
    )
endif()

install(DIRECTORY ${CMAKE_BINARY_DIR}/package/
        DESTINATION "."
        USE_SOURCE_PERMISSIONS
        COMPONENT "_switch" # Note: _ prefix is necessary to make sure the Cura component is always listed first
)

if(CPACK_GENERATOR MATCHES "NSIS64" OR CPACK_GENERATOR MATCHES "NSIS")
    # Only NSIS needs to have arduino and vcredist
    # install(DIRECTORY ${EXTERNALPROJECT_INSTALL_PREFIX}/arduino
    #         DESTINATION "."
    #         COMPONENT "arduino"
    # )
    install(FILES ${EXTERNALPROJECT_INSTALL_PREFIX}/mongodb-windows-x86_64-4.4.5-signed.msi
        DESTINATION "."
        COMPONENT "MongoDb"
    )
    install(FILES ${EXTERNALPROJECT_INSTALL_PREFIX}/NanoServer.exe
            DESTINATION "."
            COMPONENT "NanoServer"
    )
    install(FILES ${CMAKE_SOURCE_DIR}/windows/vcredist_x64.exe
            DESTINATION "."
            COMPONENT "vcredist"
    )

    set(CPACK_NSIS_PACKAGE_ARCHITECTURE "64")

    include(packaging/cpackconfig_nsis.cmake)
    include(CPack)

    add_custom_command(
        TARGET build_bundle POST_BUILD
        # NOTE: Needs testing here, whether CPACK_SYSTEM_NAME is working good for 64bit builds, too.
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/NSIS "${CMAKE_CURRENT_BINARY_DIR}/_CPack_Packages/${CPACK_SYSTEM_NAME}/NSIS"
        COMMENT "Copying NSIS scripts from [${CMAKE_SOURCE_DIR}/NSIS] to [${CMAKE_CURRENT_BINARY_DIR}/_CPack_Packages/${CPACK_SYSTEM_NAME}/NSIS]"
    )

elseif(CPACK_GENERATOR MATCHES "WIX")
    include(packaging/cpackconfig_wix.cmake)
    include(CPack)
endif()
