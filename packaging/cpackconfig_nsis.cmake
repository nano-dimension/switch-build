include(CPackComponent)

include(packaging/cpackconfig_common.cmake)

# Only include Arduino driver and VC runtime redistribution installer for NSIS
cpack_add_component(vcredist DISPLAY_NAME "Install Visual Studio 2015 Redistributable")

cpack_add_component(MongoDB DISPLAY_NAME "Install DataBase")
cpack_add_component(NanoServer DISPLAY_NAME "Install FLIGHT Control Server")
cpack_add_component(arduino DISPLAY_NAME "Arduino Drivers")

# ========================================
# NSIS
# ========================================
set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
set(CPACK_NSIS_EXECUTABLES_DIRECTORY ".")
set(CPACK_NSIS_STARTMENU_DIRECTORY "FLIGHT Control")
set(CPACK_NSIS_INSTALLED_ICON_NAME "switch.ico")
set(CPACK_NSIS_MUI_ICON ${CMAKE_SOURCE_DIR}\\\\packaging\\\\switch.ico)   # note: fails with forward '/'
set(CPACK_PACKAGE_ICON ${CMAKE_SOURCE_DIR}\\\\packaging\\\\switch.ico)

set(CPACK_NSIS_MENU_LINKS
    "https://www.nano-di.com/" "Online Documentation"
    "https://www.nano-di.com/" "Development Resources"
)

set(CPACK_NSIS_MUI_WELCOMEFINISHPAGE_BITMAP ${CMAKE_SOURCE_DIR}\\\\packaging\\\\machine.bmp)    # note: fails with forward '/'
set(CPACK_NSIS_MUI_UNWELCOMEFINISHPAGE_BITMAP ${CMAKE_SOURCE_DIR}\\\\packaging\\\\machine.bmp)
set(CPACK_NSIS_MUI_HEADERIMAGE ${CMAKE_SOURCE_DIR}\\\\packaging\\\\logo.bmp)
# set(CPACK_NSIS_INSTALLER_MUI_FINISHPAGE_RUN_CODE "!define MUI_FINISHPAGE_RUN \\\"$WINDIR\\\\explorer.exe\\\"\n!define MUI_FINISHPAGE_RUN_PARAMETERS \\\"$INSTDIR\\\\FLIGHT Control.exe\\\"")	# Hack to ensure FLIGHT Control is not started with admin rights
